```bash
Kelompok    : 5
Kelas       : Advance Programming - B
Anggota     : Aloysius K. M. (1706040025)
              Aulia Rosyida (1706025346)
              Fakhira Devina (1706979221)
              Ferro Geraldi H. (1706028612)
              M. Feril Bagus P. (1706075054)

```

## Link Heroku
Auth-Lelang<br>
http://auth-lelang.herokuapp.com/

## Status Aplikasi
[![Pipeline](https://gitlab.com/kel-5-advpro/auth-lelang/badges/master/pipeline.svg)](https://gitlab.com/kel-5-advpro/auth-lelang/commits/master)
[![Coverage](https://gitlab.com/kel-5-advpro/auth-lelang/badges/master/coverage.svg)](https://kel-5-advpro.gitlab.io/auth-lelang/index.html)

## Deskripsi Projek <br>
http://bit.ly/descGoLelang

 