package com.authlelang.authentication;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import com.authlelang.authentication.model.User;
import com.authlelang.authentication.repository.UserRepository;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AuthenticationApplication.class, properties = "spring.profiles.active=test", webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

public class AuthenticationApplicationTests {

	@Autowired
	private UserRepository userRepository;

	@LocalServerPort
	private int port;

	@Before
	public void prepare() {
		RestAssured.baseURI = "http://localhost:" + port;
		User user = userRepository.findByUsername("gogo3");
		if (user != null) {
			userRepository.delete(user);
		}
	}

	@Test
	public void authFlow() {
		JSONObject payload = new JSONObject();
		JSONObject wrongPayload = new JSONObject();
		try {
			payload.put("username", "gogo3");
			payload.put("password", "123");
			payload.put("email", "gogo@email.com");
			wrongPayload.put("username", "gogo3");
			wrongPayload.put("password", "1234");
		} catch (Exception e) {
			System.out.println(e);
			return;
		}

		// Auth Flow
		// Test register
		RestAssured.given().log().all().contentType(ContentType.JSON).body(payload.toString())
				.header("Accept", ContentType.JSON.getAcceptHeader()).when().post("/register").then().assertThat()
				.statusCode(200);

		// Test wrong login
		RestAssured.given().log().all().contentType(ContentType.JSON).body(wrongPayload.toString())
				.header("Accept", ContentType.JSON.getAcceptHeader()).when().post("/login").then().assertThat()
				.statusCode(400);

		// Test login
		payload.remove("email");
		String token = RestAssured.given().log().all().contentType(ContentType.JSON).body(payload.toString())
				.header("Accept", ContentType.JSON.getAcceptHeader()).when().post("/login").then().assertThat()
				.statusCode(200).extract().path("token");

		// Test validate token
		RestAssured.given().log().all().contentType(ContentType.JSON).header("x-auth-token", token).get("/api/profile")
				.then().assertThat().statusCode(200);

		// Test validate false token
		token = "abc";
		RestAssured.given().log().all().contentType(ContentType.JSON).header("x-auth-token", token).get("/api/profile")
				.then().assertThat().statusCode(401);
	}

	@Test
	public void testUserCrud() {
		JSONObject payload = new JSONObject();
		try {
			payload.put("username", "gogo3");
			payload.put("password", "123");
			payload.put("email", "gogo@email.com");
			payload.put("avaUser", "huehue.gif");
			payload.put("admin", false);
			payload.put("bio", "Aku adalah seorang pelaut");
		} catch (Exception e) {
			System.out.println(e);
			return;
		}

		RestAssured.given().log().all().contentType(ContentType.JSON).get("/api/users").then().assertThat()
				.statusCode(200);

		RestAssured.given().log().all().contentType(ContentType.JSON).get("/api/users/{username}", "gogo3").then()
				.assertThat().statusCode(200);

		RestAssured.given().log().all().contentType(ContentType.JSON).body(payload.toString()).post("/api/users").then()
				.assertThat().statusCode(200);
	}
}
