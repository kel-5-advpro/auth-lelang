package com.authlelang.authentication.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import com.authlelang.authentication.model.User;
import com.authlelang.authentication.repository.UserRepository;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User findUserByUserName(String username) {
        return userRepository.findByUsername(username);
    }

    public Boolean authenticate(String username, String password) {
        User user = findUserByUserName(username);
        if (null != user) {
            return user.getPassword().equals(password);
        }
        return false;
    }
}