package com.authlelang.authentication.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.authlelang.authentication.model.AuthForm;
import com.authlelang.authentication.model.Credentials;
import com.authlelang.authentication.model.JwtUser;
import com.authlelang.authentication.model.User;
import com.authlelang.authentication.repository.UserRepository;
import com.authlelang.authentication.services.JwtService;
import com.authlelang.authentication.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserRepository userRepository;

    @Value("${jwt.auth.header}")
    String authHeader;

    @PostMapping(value = "/login")
    public ResponseEntity<?> auth(@RequestBody AuthForm auth) {
        String username = auth.getUsername();
        String password = auth.getPassword();
        Boolean correctCredentials = userService.authenticate(username, password);
        if (correctCredentials) {
            JwtUser jwtUser = new JwtUser(username, password);
            Credentials credentials = new Credentials(username, jwtService.getToken(jwtUser));
            return ResponseEntity.ok(credentials);
        }
        return new ResponseEntity<>("Wrong username and password", HttpStatus.BAD_REQUEST);
    }

    @PostMapping(value = "/register")
    public User createUser(@Valid @RequestBody User user) {
        return userRepository.save(user);
    }

    @GetMapping(value = "/api/profile")
    public ResponseEntity<?> getProfile(@RequestHeader(value = "x-auth-token") String token,
            HttpServletRequest request) {
        try {
            JwtUser jwtuser = jwtService.getUser(token);
            User user = userRepository.findByUsername(jwtuser.getUsername());
            return ResponseEntity.ok(user);
        } catch (Exception e) {
            return new ResponseEntity<>("False Token", HttpStatus.UNAUTHORIZED);
        }
    }
}