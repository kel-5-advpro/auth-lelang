package com.authlelang.authentication.controller;

import javax.validation.Valid;

import com.authlelang.authentication.model.User;
import com.authlelang.authentication.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Value("${jwt.auth.header}")
    String authHeader;

    @GetMapping("/api/users")
    public Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/api/users/{username}")
    public User getUser(@PathVariable String username) {
        return userRepository.findByUsername(username);
    }

    @PostMapping("/api/users")
    public User createUser(@Valid @RequestBody User user) {
        return userRepository.save(user);
    }
}