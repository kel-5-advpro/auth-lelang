package com.authlelang.authentication.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank(message = "Username is required")
    @Column(updatable = false, unique = true, nullable = false)
    private String username;

    @NotNull(message = "Password is required")
    private String password;
    private String email;
    private String bio;
    private String avaUser;
    private boolean isAdmin;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date joinedAt;

    public User(String username, String password, String email, String bio, String avaUser, boolean isAdmin) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.isAdmin = isAdmin;
    }

    @PrePersist
    public void onCreate() {
        this.joinedAt = new Date();
    }
}