package com.authlelang.authentication.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class AuthForm {
    private String username;
    private String password;
}